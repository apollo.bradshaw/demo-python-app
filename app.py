from flask import Flask, render_template
app = Flask(__name__)
@app.route("/")
def hello():
    return "Hello Canberra!"

@app.route('/bus')
def bus_stop():
    return render_template('bus_stop.html')

if __name__ == "__main__":
    app.run()
